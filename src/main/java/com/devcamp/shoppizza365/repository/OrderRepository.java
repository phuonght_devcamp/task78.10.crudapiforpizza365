package com.devcamp.shoppizza365.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.devcamp.shoppizza365.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	Order findById(String id);
	
}

