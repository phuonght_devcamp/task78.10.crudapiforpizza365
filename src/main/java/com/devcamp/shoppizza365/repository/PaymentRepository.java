package com.devcamp.shoppizza365.repository;



import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
	Payment findById(String id);
	
}

