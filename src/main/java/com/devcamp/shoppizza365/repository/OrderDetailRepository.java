package com.devcamp.shoppizza365.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.model.OrderDetail;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
	OrderDetail findById(String id);
	OrderDetail findByOrdersAndProducts(String id1,String id2);
	
}

