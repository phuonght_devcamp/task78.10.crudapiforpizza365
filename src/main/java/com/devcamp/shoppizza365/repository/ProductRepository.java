package com.devcamp.shoppizza365.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.model.Products;

@Repository
public interface ProductRepository extends JpaRepository<Products, Long> {
	Products findById(String id);
	
}

