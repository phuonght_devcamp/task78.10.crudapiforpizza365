package com.devcamp.shoppizza365.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Payment;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.PaymentRepository;

import java.util.*;

@RestController
public class PaymentController {
	@Autowired
	private PaymentRepository paymentRepository;
	
	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping("/payment/all")
	public List<Payment> getAllPayment() {
		return paymentRepository.findAll();
	}
	
	
	@PostMapping("/payment/create/{id}")
	public ResponseEntity<Object> createPayment(@PathVariable("id") Long id, @RequestBody Payment cPayment) {
		Optional<Customer> _customerData = customerRepository.findById(id);
		if (_customerData.isPresent())
		{
			Payment newRole = new Payment();
			newRole.setAmmount(cPayment.getAmmount());
			
			newRole.setCheckNumber(cPayment.getCheckNumber());
			newRole.setPaymentDate(cPayment.getPaymentDate());
			
			
			Customer _customer = _customerData.get();
			newRole.setCustomer(_customer);
			
						
			Payment savedRole = paymentRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/payment/update/{id}")
	public ResponseEntity<Object> updatePayment(@PathVariable("id") Long id, @RequestBody Payment cPayment) {
		Optional<Payment> paymentData = paymentRepository.findById(id);
		if (paymentData.isPresent()) {
			Payment newPayment = paymentData.get();
			newPayment.setAmmount(cPayment.getAmmount());
			
			newPayment.setCheckNumber(cPayment.getCheckNumber());
			newPayment.setPaymentDate(cPayment.getPaymentDate());
			
			
			Payment savedPayment = paymentRepository.save(newPayment);
			return new ResponseEntity<>(savedPayment, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/payment/delete/{id}")
	public ResponseEntity<Object> deletePaymentById(@PathVariable Long id) {
		try {
			paymentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/payment/details/{id}")
	public Payment getPaymentById(@PathVariable Long id) {
		if (paymentRepository.findById(id).isPresent())
			return paymentRepository.findById(id).get();
		else
			return null;
	}

	

//	@Autowired
//	private RegionRepository regionRepository;
//
//	@GetMapping("/regions/{PaymentCode}")
//	public ResponseEntity<List<CRegion>> getRegionsByPaymentCode(@PathVariable("PaymentCode") String PaymentCode) {
//		try {
//			Payment vPayment = PaymentRepository.findByPaymentCode(PaymentCode);
//
//			if (vPayment != null) {
//				return new ResponseEntity<>(vPayment.getRegions(), HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@GetMapping("/Payment/{PaymentId}/regions")
//	public List<CRegion> getRegionsByPayment(@PathVariable(value = "PaymentId") Long PaymentId) {
//		return regionRepository.findByPaymentId(PaymentId);
//	}
//
//	@GetMapping("/Payment/{PaymentId}/regions/{id}")
//	public Optional<CRegion> getRegionByRegionAndPayment(@PathVariable(value = "PaymentId") Long PaymentId,
//			@PathVariable(value = "id") Long regionId) {
//		return regionRepository.findByIdAndPaymentId(regionId, PaymentId);
//	}
//
//	@GetMapping("/region/details/{id}")
//	public CRegion getRegionById(@PathVariable Long id) {
//		if (regionRepository.findById(id).isPresent())
//			return regionRepository.findById(id).get();
//		else
//			return null;
//	}
//
//	@GetMapping("/region/all")
//	public List<CRegion> getAllRegions() {
//		return regionRepository.findAll();
//	}
//
//	@GetMapping("/Payment/{PaymentCode}/{regionCode}")
//	public Optional<CRegion> getRegionByPaymentCodeAndRegionCode(
//			@PathVariable(value = "PaymentCode") String PaymentCode,
//			@PathVariable(value = "regionCode") String regionCode) {
//		Optional<CRegion> optional = regionRepository.findByRegionCodeAndPaymentPaymentCode(regionCode,PaymentCode);
//		if (optional.isPresent()) {
//			return optional;
//		} else {
//			return null;
//		}
//	}
//
//	@GetMapping("/region/code/{regionCode}")
//	public CRegion getRegionByCode(@PathVariable(value = "regionCode") String regionCode) {
//		return regionRepository.findByRegionCode(regionCode);
//	}
//	
//	@GetMapping("/getPayment/{name}")
//	public List<Payment> getPayment ( @ PathVariable(value="name") String name){
//			
//		List<Payment> optional = PaymentRepository.findPaymentByPaymentNameDesc(name);
//		
//			return optional;
//		
//	}
//	
//	@GetMapping("/getPayment1/{name}")
//	public List<Payment> getPayment1 ( @ PathVariable(value="name") String name){
//			
//		List<Payment> optional = PaymentRepository.findPaymentByPaymentNameLike(name);
//		
//			return optional;
//		
//	}
//	
}
