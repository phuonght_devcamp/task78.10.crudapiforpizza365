package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Employee;
import com.devcamp.shoppizza365.model.Employee;

import com.devcamp.shoppizza365.repository.EmployeeRepository;

import java.util.*;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeRepository employeeRepository;

	@PostMapping("/employee/create")
	public ResponseEntity<Object> createEmployee(@RequestBody Employee cEmployee) {
		try {
			Employee newRole = new Employee();
			newRole.setEmail(cEmployee.getEmail());
			newRole.setExtension(cEmployee.getExtension());
			newRole.setFirstName(cEmployee.getFirstName());
			newRole.setJobTitle(cEmployee.getJobTitle());
			newRole.setLastName(cEmployee.getLastName());
			newRole.setOfficeCode(cEmployee.getOfficeCode());
			newRole.setReportTo(cEmployee.getReportTo());
			
						
			Employee savedRole = employeeRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/employee/update/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable("id") Long id, @RequestBody Employee cEmployee) {
		Optional<Employee> employeeData = employeeRepository.findById(id);
		if (employeeData.isPresent()) {
			Employee newEmployee = employeeData.get();
			
			newEmployee.setEmail(cEmployee.getEmail());
			newEmployee.setExtension(cEmployee.getExtension());
			newEmployee.setFirstName(cEmployee.getFirstName());
			newEmployee.setJobTitle(cEmployee.getJobTitle());
			newEmployee.setLastName(cEmployee.getLastName());
			newEmployee.setOfficeCode(cEmployee.getOfficeCode());
			newEmployee.setReportTo(cEmployee.getReportTo());
			
			 
	
			Employee savedEmployee = employeeRepository.save(newEmployee);
			return new ResponseEntity<>(savedEmployee, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/employee/delete/{id}")
	public ResponseEntity<Object> deleteEmployeeById(@PathVariable Long id) {
		try {
			employeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/employee/details/{id}")
	public Employee getEmployeeById(@PathVariable Long id) {
		if (employeeRepository.findById(id).isPresent())
			return employeeRepository.findById(id).get();
		else
			return null;
	}


	@GetMapping("/employee/all")
	public List<Employee> getAllEmployee() {
		return employeeRepository.findAll();
	}

//	@
}
