package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.repository.CustomerRepository;

import java.util.*;

@RestController
public class CustomerController {
	@Autowired
	private CustomerRepository customerRepository;

	@PostMapping("/customer/create")
	public ResponseEntity<Object> createCustomer(@RequestBody Customer cCustomer) {
		try {
			Customer newRole = new Customer();
			newRole.setAddress(cCustomer.getAddress());
			newRole.setCity(cCustomer.getCity());
			newRole.setCountry(cCustomer.getCountry());
			newRole.setCreditLimit(cCustomer.getCreditLimit());
			newRole.setFirstName(cCustomer.getFirstName());
			newRole.setLastName(cCustomer.getLastName());
			newRole.setOrders(cCustomer.getOrders());
			newRole.setPayments(cCustomer.getPayments());
			newRole.setPhoneNumber(cCustomer.getPhoneNumber());
			newRole.setPostalCode(cCustomer.getPostalCode());
			newRole.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			newRole.setState(cCustomer.getState());
						
			Customer savedRole = customerRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/customer/update/{id}")
	public ResponseEntity<Object> updateCustomer(@PathVariable("id") Long id, @RequestBody Customer cCustomer) {
		Optional<Customer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			Customer newCustomer = customerData.get();
			
			 newCustomer.setAddress(cCustomer.getAddress());
			 newCustomer.setCity(cCustomer.getCity());
			 newCustomer.setCountry(cCustomer.getCountry());
			 newCustomer.setCreditLimit(cCustomer.getCreditLimit());
			 newCustomer.setFirstName(cCustomer.getFirstName());
			 newCustomer.setLastName(cCustomer.getLastName());
			 newCustomer.setPhoneNumber(cCustomer.getPhoneNumber());
			 newCustomer.setPostalCode(cCustomer.getPostalCode());
			 newCustomer.setSalesRepEmployeeNumber(cCustomer.getSalesRepEmployeeNumber());
			 newCustomer.setState(cCustomer.getState());
			 newCustomer.setOrders(cCustomer.getOrders());
			 newCustomer.setPayments(cCustomer.getPayments());
			 
	
			Customer savedCustomer = customerRepository.save(newCustomer);
			return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
		try {
			customerRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
	@GetMapping("/customer/details/{id}")
	public Customer getCustomerById(@PathVariable Long id) {
		if (customerRepository.findById(id).isPresent())
			return customerRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/customer/all")
	public List<Customer> getAllCustomer() {
		return customerRepository.findAll();
	}

//	@Autowired
//	private RegionRepository regionRepository;
//
//	@GetMapping("/regions/{CustomerCode}")
//	public ResponseEntity<List<CRegion>> getRegionsByCustomerCode(@PathVariable("CustomerCode") String CustomerCode) {
//		try {
//			Customer vCustomer = CustomerRepository.findByCustomerCode(CustomerCode);
//
//			if (vCustomer != null) {
//				return new ResponseEntity<>(vCustomer.getRegions(), HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@GetMapping("/Customer/{CustomerId}/regions")
//	public List<CRegion> getRegionsByCustomer(@PathVariable(value = "CustomerId") Long CustomerId) {
//		return regionRepository.findByCustomerId(CustomerId);
//	}
//
//	@GetMapping("/Customer/{CustomerId}/regions/{id}")
//	public Optional<CRegion> getRegionByRegionAndCustomer(@PathVariable(value = "CustomerId") Long CustomerId,
//			@PathVariable(value = "id") Long regionId) {
//		return regionRepository.findByIdAndCustomerId(regionId, CustomerId);
//	}
//
//	@GetMapping("/region/details/{id}")
//	public CRegion getRegionById(@PathVariable Long id) {
//		if (regionRepository.findById(id).isPresent())
//			return regionRepository.findById(id).get();
//		else
//			return null;
//	}
//
//	@GetMapping("/region/all")
//	public List<CRegion> getAllRegions() {
//		return regionRepository.findAll();
//	}
//
//	@GetMapping("/Customer/{CustomerCode}/{regionCode}")
//	public Optional<CRegion> getRegionByCustomerCodeAndRegionCode(
//			@PathVariable(value = "CustomerCode") String CustomerCode,
//			@PathVariable(value = "regionCode") String regionCode) {
//		Optional<CRegion> optional = regionRepository.findByRegionCodeAndCustomerCustomerCode(regionCode,CustomerCode);
//		if (optional.isPresent()) {
//			return optional;
//		} else {
//			return null;
//		}
//	}
//
//	@GetMapping("/region/code/{regionCode}")
//	public CRegion getRegionByCode(@PathVariable(value = "regionCode") String regionCode) {
//		return regionRepository.findByRegionCode(regionCode);
//	}
//	
//	@GetMapping("/getCustomer/{name}")
//	public List<Customer> getCustomer ( @ PathVariable(value="name") String name){
//			
//		List<Customer> optional = CustomerRepository.findCustomerByCustomerNameDesc(name);
//		
//			return optional;
//		
//	}
//	
//	@GetMapping("/getCustomer1/{name}")
//	public List<Customer> getCustomer1 ( @ PathVariable(value="name") String name){
//			
//		List<Customer> optional = CustomerRepository.findCustomerByCustomerNameLike(name);
//		
//			return optional;
//		
//	}
//	
}
