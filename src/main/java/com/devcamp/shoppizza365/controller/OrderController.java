package com.devcamp.shoppizza365.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.OrderRepository;

import java.util.*;

@RestController
public class OrderController {
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;

	@GetMapping("/order/all")
	public List<Order> getAllOrder() {
		return orderRepository.findAll();
	}
	
	
	@PostMapping("/order/create/{id}")
	public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody Order cOrder) {
		Optional<Customer> customerData = customerRepository.findById(id);
		if (customerData.isPresent())
		{
			Order newRole = new Order();
			newRole.setComments(cOrder.getComments());
			
			newRole.setOrderDate(cOrder.getOrderDate());
			newRole.setOrderDetails(cOrder.getOrderDetails());
			newRole.setRequiredDate(cOrder.getRequiredDate());
			newRole.setShippedDate(cOrder.getShippedDate());
			newRole.setStatus(cOrder.getStatus());
			
			Customer _customer = customerData.get();
			newRole.setCustomer(_customer);
			
						
			Order savedRole = orderRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/order/update/{id}")
	public ResponseEntity<Object> updateorder(@PathVariable("id") Long id, @RequestBody Order cOrder) {
		Optional<Order> orderData = orderRepository.findById(id);
		if (orderData.isPresent()) {
			Order newOrder = orderData.get();
			newOrder.setComments(cOrder.getComments());
			
			newOrder.setOrderDate(cOrder.getOrderDate());
			newOrder.setOrderDetails(cOrder.getOrderDetails());
			newOrder.setRequiredDate(cOrder.getRequiredDate());
			newOrder.setShippedDate(cOrder.getShippedDate());
			newOrder.setStatus(cOrder.getStatus());
			
			Order savedOrder = orderRepository.save(newOrder);
			return new ResponseEntity<>(savedOrder, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
//
	@DeleteMapping("/order/delete/{id}")
	public ResponseEntity<Object> deleteorderById(@PathVariable Long id) {
		try {
			orderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/order/details/{id}")
	public Order getOrderById(@PathVariable Long id) {
		if (orderRepository.findById(id).isPresent())
			return orderRepository.findById(id).get();
		else
			return null;
	}

	

//	@Autowired
//	private RegionRepository regionRepository;
//
//	@GetMapping("/regions/{orderCode}")
//	public ResponseEntity<List<CRegion>> getRegionsByorderCode(@PathVariable("orderCode") String orderCode) {
//		try {
//			Order vorder = orderRepository.findByorderCode(orderCode);
//
//			if (vorder != null) {
//				return new ResponseEntity<>(vorder.getRegions(), HttpStatus.OK);
//			} else {
//				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
//			}
//		} catch (Exception e) {
//			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@GetMapping("/order/{orderId}/regions")
//	public List<CRegion> getRegionsByorder(@PathVariable(value = "orderId") Long orderId) {
//		return regionRepository.findByorderId(orderId);
//	}
//
//	@GetMapping("/order/{orderId}/regions/{id}")
//	public Optional<CRegion> getRegionByRegionAndorder(@PathVariable(value = "orderId") Long orderId,
//			@PathVariable(value = "id") Long regionId) {
//		return regionRepository.findByIdAndorderId(regionId, orderId);
//	}
//
//	@GetMapping("/region/details/{id}")
//	public CRegion getRegionById(@PathVariable Long id) {
//		if (regionRepository.findById(id).isPresent())
//			return regionRepository.findById(id).get();
//		else
//			return null;
//	}
//
//	@GetMapping("/region/all")
//	public List<CRegion> getAllRegions() {
//		return regionRepository.findAll();
//	}
//
//	@GetMapping("/order/{orderCode}/{regionCode}")
//	public Optional<CRegion> getRegionByorderCodeAndRegionCode(
//			@PathVariable(value = "orderCode") String orderCode,
//			@PathVariable(value = "regionCode") String regionCode) {
//		Optional<CRegion> optional = regionRepository.findByRegionCodeAndorderorderCode(regionCode,orderCode);
//		if (optional.isPresent()) {
//			return optional;
//		} else {
//			return null;
//		}
//	}
//
//	@GetMapping("/region/code/{regionCode}")
//	public CRegion getRegionByCode(@PathVariable(value = "regionCode") String regionCode) {
//		return regionRepository.findByRegionCode(regionCode);
//	}
//	
//	@GetMapping("/getorder/{name}")
//	public List<Order> getorder ( @ PathVariable(value="name") String name){
//			
//		List<Order> optional = orderRepository.findorderByorderNameDesc(name);
//		
//			return optional;
//		
//	}
//	
//	@GetMapping("/getorder1/{name}")
//	public List<Order> getorder1 ( @ PathVariable(value="name") String name){
//			
//		List<Order> optional = orderRepository.findorderByorderNameLike(name);
//		
//			return optional;
//		
//	}
//	
}
