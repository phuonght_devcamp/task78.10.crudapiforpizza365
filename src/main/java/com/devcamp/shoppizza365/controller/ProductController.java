package com.devcamp.shoppizza365.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.shoppizza365.model.Customer;
import com.devcamp.shoppizza365.model.Order;
import com.devcamp.shoppizza365.model.ProductLines;

import com.devcamp.shoppizza365.model.Products;
import com.devcamp.shoppizza365.repository.CustomerRepository;
import com.devcamp.shoppizza365.repository.ProductLineRepository;
import com.devcamp.shoppizza365.repository.ProductRepository;

import java.util.*;

@RestController
public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ProductLineRepository productLineRepository;

	@PostMapping("/product/create/{id}")
	public ResponseEntity<Object> createProduct(@PathVariable("id") Long id, @RequestBody Products cProduct) {
		Optional<ProductLines> productLineData = productLineRepository.findById(id);
		if (productLineData.isPresent())
		{
			Products newRole = new Products();
			newRole.setProductCode(cProduct.getProductCode());
			
			newRole.setBuyPrice(cProduct.getBuyPrice());
			newRole.setProductDescription(cProduct.getProductDescription());
			newRole.setProductName(cProduct.getProductName());
			newRole.setProductScale(cProduct.getProductScale());
			newRole.setProductVendor(cProduct.getProductVendor());
			
			ProductLines _prdLine = productLineData.get();
			newRole.setProductLine(_prdLine);
			
						
			Products savedRole = productRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@PutMapping("/product/update/{id}")
	public ResponseEntity<Object> updateProd(@PathVariable("id") Long id, @RequestBody Products cProduct) {
		Optional<Products> prodData = productRepository.findById(id);
		if (prodData.isPresent()) {
			Products newProd = prodData.get();
			newProd.setProductCode(cProduct.getProductCode());
						
			newProd.setBuyPrice(cProduct.getBuyPrice());
			newProd.setProductDescription(cProduct.getProductDescription());
			newProd.setProductName(cProduct.getProductName());
			newProd.setProductScale(cProduct.getProductScale());
			newProd.setProductVendor(cProduct.getProductVendor());
			
			Products savedProd = productRepository.save(newProd);
			return new ResponseEntity<>(savedProd, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@DeleteMapping("/product/delete/{id}")
	public ResponseEntity<Object> deleteProdById(@PathVariable Long id) {
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/product/details/{id}")
	public Products getProdById(@PathVariable Long id) {
		if (productRepository.findById(id).isPresent())
			return productRepository.findById(id).get();
		else
			return null;
	}

	@GetMapping("/product/all")
	public List<Products> getAllProduct() {
		return productRepository.findAll();
	}

//	
}
