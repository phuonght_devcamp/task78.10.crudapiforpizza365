package com.devcamp.shoppizza365.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="products")
public class Products {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="product_code")
	private String productCode;
	
	@Column(name="product_name")
	private String productName;
	
	@Column(name="product_description")
	private String productDescription;
	

	@Column(name="product_scale")
	private String productScale;

	@Column(name="product_vendor")
	private String productVendor;
	
	
	@Column(name="quantity_in_stock")
	private int quantityInStock;
	
	
	@Column(name="buy_price")
	private BigDecimal buyPrice;
	
	@OneToMany(mappedBy="products",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OrderDetail> orderDetails;
	
	@ManyToOne
	@JoinColumn(name = "product_line_id", nullable = false)
	@JsonIgnore 
	private ProductLines productLine;

	public Products(int id, String productCode, String productName, String productDescription, String productScale,
			String productVendor, int quantityInStock, BigDecimal buyPrice, List<OrderDetail> orderDetails,
			ProductLines productLine) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.orderDetails = orderDetails;
//		this.productLine = productLine;
	}

	public Products() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductScale() {
		return productScale;
	}

	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public int getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}
//
	public ProductLines getProductLine() {
		return productLine;
	}

	public void setProductLine(ProductLines productLine) {
		this.productLine = productLine;
	}
	
	

	
}
