package com.devcamp.shoppizza365.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="product_lines")
public class ProductLines {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="product_line")
	private String productLine;
	
	@Column(name="description")
	private String description;
	
	@OneToMany(mappedBy="productLine",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Products> products;

	public ProductLines(int id, String productLine, String description, List<Products> products) {
		super();
		this.id = id;
		this.productLine = productLine;
		this.description = description;
		this.products = products;
	}

	public ProductLines() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}


	
	

}
